package com.dgss.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import brave.sampler.Sampler;

@EnableDiscoveryClient
@SpringBootApplication
public class DgssUserApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DgssUserApiApplication.class, args);
	}
	
	@Bean
	public Sampler defautlSamler() {
		return Sampler.ALWAYS_SAMPLE;
	}

}
