package com.dgss.user.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
	
	Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@GetMapping("/name")
	public ResponseEntity<String> getEmployeeName(){
		logger.info("Our First rest api");
		return new ResponseEntity<String>("Hello Amol", HttpStatus.OK);
	}

}
